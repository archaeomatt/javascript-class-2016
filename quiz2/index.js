
 

    var gamePieces = new Array ("rock", "paper", "scissors", "dynamite"); 

    //tieing what the user has input with one of the options in the array listed above
    var textValueFromTxtInput = document.getElementById("playerChoice");


    //below is the random number generator that is linked to the array coded above
    function getRandomGamePiece(arraySize){
        // obtain a random game piece for the computer
        var rnd = Math.floor(Math.random() * arraySize);
        // output computer's gamepiece
        return rnd;
    }
    //below is a switch that relates the random number generated for the computer's
    //game piece that will be used to print out what piece the computer has used.        
    function opponentPiece(num){ 
        switch(num){
            case 0: // if newDate().getDay() = 0
                return "rock";
                break;
            case 1: 
                return "paper";
                break; 
            case 2: // if newDate().getDay() = 2
                return "scissors";
                break;
            case 3:
                return "dynamite";
                break;                               
        }
    };
    //below is the function for displaying and determining who won the game.
    function whoWins(playerChoice, comChoice) {
        if (playerChoice == "rock") {
            switch(comChoice){
                case 0: //rock
                    return "tie";
                    break;
                case 1: //paper
                    return "paper wins";
                    break; 
                case 2: //scissors
                    return "rock wins";
                    break;
                case 3: //dynamite
                    return "dynamite wins";
                    break;
            }                               
        }
        else if (playerChoice == "scissors") {
            switch(comChoice){
                case 0: //rock
                    return " lose";
                    break;
                case 1: //paper
                    return " win";
                    break; 
                case 2: //scissors
                    return "tie";
                    break;
                case 3: //dynamite
                    return " win";
                    break;
            }                               
        }
        else if (playerChoice == "paper") {
            switch(comChoice){
                case 0: //rock
                    return " win";
                    break;
                case 1: //paper
                    return " tie";
                    break; 
                case 2: //scissors
                    return "lose";
                    break;
                case 3: //dynamite
                    return " lose";
                    break;
            }                               
        }
        else if (playerChoice == "dynamite") {
            switch(comChoice){
                case 0: //rock
                    return " win";
                    break;
                case 1: //paper
                    return " win";
                    break; 
                case 2: //scissors
                    return "lose";
                    break;
                case 3: //dynamite
                    return " tie";
                    break;
            }                               
        }
    }

    // below is the jquery script for returning the results as determined from the if/switch above
    $("#playerChoice").click(function(){
        var playerPiece = prompt('Choose your weapon (rock, paper, scissors, or dynamite)');
        var opponenentChoice = getRandomGamePiece(gamePieces.length);
        $("#results").text("You chose " + playerPiece + " and your oponent chose " + opponentPiece(opponenentChoice) + ". You " + whoWins(playerPiece, opponenentChoice));
    });   

