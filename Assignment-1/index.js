 


//the below function is for the dropdown list of the Canadian provinces and territories
 function loadProvinces(){
    var provArray = ["-Select-", "British Columbia", "Alberta", "Saskatchewan", "Manitoba", 
    	"Ontario", "Quebec", "New Brunsick", "Nova Scotia", "Newfoundland", "Prince Edward Island"];
    var optionOutput="";

    //document.write("<select name='cboProv'>");
    document.getElementById("<option name='provArray[0]' value=''> </option>");

    for (i=0; i<provArray.length; i++){
    	optionOutput+= "<option name='opt" + i + "' value='" + provArray[i] +"'> "+ provArray[i]+ "</option>";
    }
    document.getElementById("cboProv").innerHTML=optionOutput;
}


 function validateForm(){
 	if(document.getElementById('cboProv').value=="-Select-"){
    	alert("Do as commanded! Choose your province or territory!");
    	document.getElementById('cboProv').focus();
    	return;
    }
    else if(document.getElementById('Name').value==""){
        alert("Do as commanded! Enter thy name!");
        document.getElementById('Name').focus();
        return;
    }
    else if(document.getElementById('Email').value==""){
        alert('Do as commanded! Enter thy email!');
        document.getElementById('Email').focus();
        return;
    }
    else 
    	alert('Congrats, you win a province or territory of your choice!');
}
